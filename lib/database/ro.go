package database

import (
	"errors"
	"time"
)

type roDB struct {
	db DB
}

func (db *roDB) Close() error {
	return db.db.Close()
}

func (db *roDB) AddBook(book Book) error {
	return errors.New("RO database")
}

func (db *roDB) GetBooks(query string, length int, start int) (books []Book, num int, err error) {
	return db.db.GetBooks(query, length, start)
}

func (db *roDB) GetNewBooks(query string, length int, start int) (books []Book, num int, err error) {
	return db.db.GetNewBooks(query, length, start)
}

func (db *roDB) GetBookID(id string) (Book, error) {
	return db.db.GetBookID(id)
}

func (db *roDB) DeleteBook(id string) error {
	return errors.New("RO database")
}

func (db *roDB) UpdateBook(id string, data map[string]interface{}) error {
	return errors.New("RO database")
}

func (db *roDB) ActiveBook(id string) error {
	return errors.New("RO database")
}

func (db *roDB) IsBookActive(id string) bool {
	return db.db.IsBookActive(id)
}

func (db *roDB) ExistsBookHash(hash []byte) bool {
	return db.db.ExistsBookHash(hash)
}

func (db *roDB) AddUser(name string, pass string) error {
	return errors.New("RO database")
}

func (db *roDB) AddRawUser(name string, hpass []byte, salt []byte, role string) error {
	return errors.New("RO database")
}

func (db *roDB) SetAdminUser(name string, pass string) error {
	return errors.New("RO database")
}

func (db *roDB) GetRole(name string) (string, error) {
	return db.db.GetRole(name)
}

func (db *roDB) ValidPassword(name string, pass string) bool {
	return db.db.ValidPassword(name, pass)
}

func (db *roDB) SetPassword(name string, pass string) error {
	return errors.New("RO database")
}

func (db *roDB) SetRole(name, role string) error {
	return errors.New("RO database")
}

func (db *roDB) ListUsers() ([]User, error) {
	return db.db.ListUsers()
}

func (db *roDB) AddNews(text string) error {
	return errors.New("RO database")
}

func (db *roDB) AddRawNews(text string, date time.Time) error {
	return errors.New("RO database")
}

func (db *roDB) GetNews(num int, days int) (news []New, err error) {
	return db.db.GetNews(num, days)
}

func (db *roDB) IncViews(ID string) error {
	return nil
}

func (db *roDB) IncDownloads(ID string) error {
	return nil
}

func (db *roDB) GetDownloadCounter(ID string) (int, error) {
	return db.db.GetDownloadCounter(ID)
}

func (db *roDB) GetFrontPage() FrontPage {
	return db.db.GetFrontPage()
}

func (db *roDB) AddSubmission(submission Submission, userName string) (id int, err error) {
	return 0, errors.New("RO database")
}

func (db *roDB) UpdateSubmission(id int, status string, book *Book) error {
	return errors.New("RO database")
}

func (db *roDB) UpdateSubmissionComment(submissionID, bookdID, comment string) error {
	return errors.New("RO database")
}

func (db *roDB) UpdateSubmissionByBook(bookID string, status string, book *Book) error {
	return errors.New("RO database")
}

func (db *roDB) GetComment(bookID string) (string, error) {
	return db.db.GetComment(bookID)
}

func (db *roDB) GetSubmission(submissionID string) (submission []Submission, err error) {
	return db.db.GetSubmission(submissionID)
}

func (db *roDB) GetUserSubmissions(userName string) (submission []Submission, err error) {
	return db.db.GetUserSubmissions(userName)
}

func (db *roDB) NewBookList(listID, title, username string, description []string) error {
	return errors.New("RO database")
}

func (db *roDB) AddBookToList(listID, bookID string) error {
	return errors.New("RO database")
}

func (db *roDB) DeleteBookFromList(listID, bookID string) error {
	return errors.New("RO database")
}

func (db *roDB) UpdateBookList(listID, title string, description []string) error {
	return errors.New("RO database")
}

func (db *roDB) GetBookList(listID string) (*BookList, error) {
	return db.db.GetBookList(listID)
}

func (db *roDB) GetListsByUser(username string) ([]BookList, error) {
	return db.db.GetListsByUser(username)
}

func (db *roDB) GetListsByBook(bookID string) ([]BookList, error) {
	return db.db.GetListsByBook(bookID)
}
